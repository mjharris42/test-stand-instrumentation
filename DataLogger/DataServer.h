#ifndef DATA_SERVER_H
#define DATA_SERVER_H

#include <list>
#include <pthread.h>

class DataServer
{
public:
    
    DataServer(int port);
    ~DataServer();

    int getStatus() const;
    
    void start();
    
    int publish(char* str);

    int listen();

private:

    int m_port;

    int m_status;

    pthread_t m_thread;

    pthread_mutex_t m_mutex;

    std::list<int> m_connections;

};

#endif
