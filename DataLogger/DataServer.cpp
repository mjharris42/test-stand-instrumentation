#include <iostream>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>

#include "DataServer.h"
#include "TcpUtils.h"

using namespace std;

static void* threadHelper(void* arg);

DataServer::DataServer(int port) 
{
    m_port = port;
    m_status = 0;
}

DataServer::~DataServer()
{
}

int DataServer::getStatus() const
{
    return m_status;
}

void DataServer::start()
{
    int status = pthread_create(&m_thread, 0, &threadHelper, this);
    if (status != 0)
    {
        cerr << "Error starting data server thread" << endl;
    }
}

int DataServer::publish(char* str)
{

    // cout << "Publish: " << str << endl;
    char buffer[128];
    sprintf(buffer, "%s\n\0", str);
    pthread_mutex_lock(&m_mutex);
    for (list<int>::iterator i = m_connections.begin(); i != m_connections.end();)
    {
        int clientSocket = *i;
        // cout << "Publishing to connection " << clientSocket << endl;
        int result = TcpUtils::sendToPeer(clientSocket, buffer, strlen(buffer) + 1, MSG_NOSIGNAL);
        // cout << "result=" << result << endl;
        if (result == -1)
        {
            TcpUtils::closeSocket(clientSocket);
            i = m_connections.erase(i);
        }
        else
        {
            i++;
        }
    }
    pthread_mutex_unlock(&m_mutex);

    return 0;
}

int DataServer::listen()
{
    int serverSocket = TcpUtils::createServerSocket(m_port);
    if (serverSocket == -1)
    {
        cerr << "Could not create server socket, port=" << m_port << endl;
        return -1;
    }

    int result = TcpUtils::listenForConnections(serverSocket);
    if (result == -1)
    {
        cerr << "Could not listen for connections on port " << m_port << endl;
        TcpUtils::closeSocket(serverSocket);
        return -1;
    }

    cout << "Data server is listening on port " << m_port << endl;

    while (1)
    {
        cout << "Data server is listening for connections" << endl;
        int clientSocket = TcpUtils::acceptConnection(serverSocket);
        if (clientSocket == -1)
        {
            cerr << "Could not accept connection" << endl;
            sleep(10);
        }
        else
        {
            cout << "Client connected, socket=" << clientSocket << endl;

            // Add the client socket to the connections list.
            pthread_mutex_lock(&m_mutex);
            m_connections.push_back(clientSocket);
            pthread_mutex_unlock(&m_mutex);
        }
    }

    TcpUtils::closeSocket(serverSocket);

    return 0;
}

static void* threadHelper(void* arg)
{
    DataServer* dataServer=static_cast<DataServer *>(arg);
    dataServer->listen();
}

