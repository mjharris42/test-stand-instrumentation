#include <iostream>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>

#include "SerialBuffer.h"
#include "SensorData.h"
#include "SensorRecord.h"
#include "DataServer.h"

using namespace std;

#define BAUD 9600 // Baud rate of the arduino.

// Function prototypes
int openSerialPort(char* portName);
FILE* openFile(char* fileName);
void readWriteLoop(int serialHandle, FILE* fd, DataServer* dataServer);
void processSensorData(SensorData_t* sensorData, FILE* fd, DataServer* dataServer);
float convertLoad(int16_t rawValue);
float convertPressure(int16_t rawValue);
float convertTemperature(int16_t rawValue);
int serializeData(SensorRecord_t* sensorRecord, char* str, size_t size);
int writeFile(FILE* fd, char* str);
int formatTimestamp(struct timeval* tv, char* buffer, size_t bufferSize);

// Serial port code adapted from https://github.com/todbot/arduino-serial
int openSerialPort(char* portName)
{
    struct termios toptions;
    int handle;
    
    handle = open(portName, O_RDWR | O_NONBLOCK );
    
    if (handle == -1)  
    {
        cerr << "Unable to open port " << portName << endl;
        return -1;
    }
    
    //int iflags = TIOCM_DTR;
    //ioctl(handle, TIOCMBIS, &iflags);     // turn on DTR
    //ioctl(handle, TIOCMBIC, &iflags);    // turn off DTR

    if (tcgetattr(handle, &toptions) < 0) 
    {
        cerr << "Unable to get " << portName << " attributes" << endl;
        return -1;
    }
    speed_t baudRate = BAUD; // let you override switch below if needed
    
    cfsetispeed(&toptions, baudRate);
    cfsetospeed(&toptions, baudRate);

    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    //toptions.c_cflag &= ~HUPCL; // disable hang-up-on-close to avoid reset

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 0;
    //toptions.c_cc[VTIME] = 20;
    
    tcsetattr(handle, TCSANOW, &toptions);
    if (tcsetattr(handle, TCSAFLUSH, &toptions) < 0) 
    {
        cerr << "Unable to set " << portName << " attributes" << endl;
        return -1;
    }

    return handle;
}

FILE* openFile(char* fileName)
{
    FILE* fd = fopen(fileName, "a+");
    if (fd == NULL)
    {
        cerr << "Could not open " << fileName << endl;
    }

    return fd;
}

void readWriteLoop(int serialHandle, FILE* fd, DataServer* dataServer)
{
    byte buffer[sizeof(SensorData_t)];

    SerialBuffer serialBuffer;

    // Set up the buffer storage and maximum size.
    serialBuffer.buffer = buffer;
    serialBuffer.bufferSize = sizeof(buffer);

    serialBuffer.reset();

    while (true)
    {
        byte inputByte = 0;
        ssize_t bytesRead = read(serialHandle, &inputByte, sizeof(inputByte));
        if (bytesRead > 0)
        {
            int bufferStatus = serialBuffer.receive(inputByte);

            /*
                SerialBuffer::receive doesn't work as exactly as documented 
                in README.mdown as it would accept an undersized message.
                Thus the check for the expected size below.
            */
            if (bufferStatus == sizeof(SensorData_t))
            {
                SensorData_t* sensorData = (SensorData_t*) buffer;
                processSensorData(sensorData, fd, dataServer);
            }
        }
    }
}

void processSensorData(SensorData_t* sensorData, FILE* fd, DataServer* dataServer)
{
    SensorRecord_t sensorRecord;

    struct timeval timestamp;
    gettimeofday(&timestamp, NULL); 
    sensorRecord.timestamp = timestamp; 

    /* Just copies raw values staight to engineering values. No conversion.
    for (int i = 0; i < CHANNELS; ++i)
    {
        sensorRecord.sensorValues.values[i] = sensorData->values[i];
    }
    */

    // Convert loads from raw integer values to float engineering values.
    for (int i = 0; i < LOAD_CHANNELS; ++i)
    {
        float engineeringValue = convertLoad(sensorData->loads[i]);
        sensorRecord.sensorValues.loads[i] = engineeringValue;
    }

    // Convert pressures from raw integer values to float engineering values.
    for (int i = 0; i < PRES_CHANNELS; ++i)
    {
        float engineeringValue = convertPressure(sensorData->pressures[i]);
        sensorRecord.sensorValues.pressures[i] = engineeringValue;
    }

    // Convert temperatures from raw integer values to float engineering values.
    for (int i = 0; i < TEMP_CHANNELS; ++i)
    {
        float engineeringValue = convertTemperature(sensorData->temperatures[i]);
        sensorRecord.sensorValues.temperatures[i] = engineeringValue;
    }

    char buffer[128];
    int written = serializeData(&sensorRecord, buffer, sizeof(buffer));
    // cout << "written=" << written << endl;
    if (written < 0)
    {
	cerr << "Error serializing data" << endl;
    }
    else if (written >= sizeof(buffer))
    {
        cerr << "Serialized data was truncated" << endl;
    }
    // cout << buffer << endl;

    written = writeFile(fd, buffer);
    // cout << "written=" << written << endl;
    if (written < 0)
    {
        cerr << "Error write serialized sensor record to file" << endl;
    }

    dataServer->publish(buffer);
}

float convertLoad(int16_t rawValue)
{
    float vRef = 5.0f;

    // We ended up preferring to the do the conversion on the python end rather than on the C++ end for
    // ease of looking at raw values and tweaking scale.
    float engineeringValue = (rawValue / 1024.f) * vRef; // In volts.

    return engineeringValue;
}

float convertPressure(int16_t rawValue)
{
    float adcMultiplier = 0.1875f;
    float engineeringValue = rawValue * adcMultiplier; // In millivolts.

    return engineeringValue;
}

float convertTemperature(int16_t rawValue)
{

    float multiplier = 0.25f;
    float engineeringValue = rawValue * multiplier;

    return engineeringValue;
}

int writeFile(FILE* fd, char* str)
{
    int written = fprintf(fd, "%s\n", str);
    fflush(fd);
    return written;
}

int serializeData(SensorRecord_t* sensorRecord, char* buffer, size_t bufferSize)
{
    int written = 0; 
    struct timeval timestamp = sensorRecord->timestamp;
    char timeBuffer[32];
    if (formatTimestamp(&timestamp, timeBuffer, sizeof(timeBuffer)) < 0)
    {
        cerr << "Error formatting timestamp" << endl;
        return 0;
    }
    int w = snprintf(buffer, bufferSize, "%s,", timeBuffer);
    if (w <= 0) return w;
    written = written + w;

    for (int i = 0; i < LOAD_CHANNELS; ++i)
    {
        // cout << sensorRecord->sensorValues.loads[i] << ",";
        int w = snprintf(buffer + written, bufferSize - written, "%.3f,", sensorRecord->sensorValues.loads[i]);
        if (w <= 0) return w;
        written = written + w;
    }

    for (int i = 0; i < PRES_CHANNELS; ++i)
    {
        // cout << sensorRecord->sensorValues.pressures[i] << ",";
        int w = snprintf(buffer + written, bufferSize - written, "%.0f,", sensorRecord->sensorValues.pressures[i]);
        if (w <= 0) return w;
        written = written + w;
    }

    for (int i = 0; i < TEMP_CHANNELS; ++i)
    {
        // cout << sensorRecord->sensorValues.temperatures[i] << ",";
        int w = snprintf(buffer + written, bufferSize - written, "%.2f,", sensorRecord->sensorValues.temperatures[i]);
        if (w <= 0) return w;
        written = written + w;
    }
    // cout << endl;

    return written;
}

// Adapted from example code found on the Internet.
int formatTimestamp(struct timeval* tv, char* buffer, size_t bufferSize)
{
    int written = 0;
    struct tm* time = localtime(&tv->tv_sec);

    if (time)
    {
        written = strftime(buffer, bufferSize, "%Y-%m-%d %H:%M:%S", time);
        if ((written > 0) && ((size_t)written < bufferSize))
        {
            suseconds_t microseconds = tv->tv_usec / 1000;
            int w = snprintf(buffer + written, bufferSize - written, ".%03d", microseconds);
            written = (w > 0) ? written + w : -1;
        }
    }

    return written;
}

int main(int argc, char* argv[])
{
    char* serialPortName = (char *) "/dev/ttyACM0";
#ifdef TIMESTAMP_FILE_NAME
    time_t now = time(NULL);
    struct tm *time = localtime(&now);
    char timeBuffer[32];
    strftime(timeBuffer, sizeof(timeBuffer), "%Y-%m-%d-%H-%M-%S", time);
    char fileName[80];
    sprintf(fileName, "serial.%s.dat", timeBuffer);
#else
    char* fileName = (char*) "serial.dat";
#endif
    int serverPort = 8888;

    if (argc >= 2) serialPortName = argv[1];
    if (argc >= 3) serverPort = atoi(argv[2]);

    DataServer dataServer(serverPort);
    dataServer.start();
    int status = dataServer.getStatus();
    cout << "dataServer status=" << status << endl;

    int serialHandle = openSerialPort(serialPortName);
    if (serialHandle < 0)
    {
        return -1;
    }

    FILE* fd = openFile(fileName);
    if (fd == NULL)
    {
	close(serialHandle);
        return -1;
    }

    readWriteLoop(serialHandle, fd, &dataServer);

    close(serialHandle);
    fclose(fd);

    return 0;
}
