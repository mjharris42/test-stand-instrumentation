#ifndef TCP_UTILS_H
#define TCP_UTILS_H

#include <sys/socket.h>

namespace TcpUtils {

int createServerSocket(int port);
int listenForConnections(int socket);
int acceptConnection(int socket);
int sendToPeer(int socket, void* buffer, size_t bufferLen, int flags);
int closeSocket(int socket);

}

#endif
