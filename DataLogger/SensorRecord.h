#ifndef SENSOR_RECORD_H
#define SENSOR_RECORD_H

#include <sys/time.h>
#include "channels.h"

typedef union 
{
    struct 
    { 
        float loads[LOAD_CHANNELS];
        float pressures[PRES_CHANNELS];
        float temperatures[TEMP_CHANNELS];
    };
    float values[CHANNELS];
} SensorValues_t;

typedef struct
{
    struct timeval timestamp;
    SensorValues_t sensorValues;
} SensorRecord_t;

#endif
