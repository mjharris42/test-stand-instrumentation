#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "TcpUtils.h"

using namespace std;

int TcpUtils::createServerSocket(int port)
{
    addrinfo hints;
    bzero(&hints, sizeof(addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    char service[64];
    snprintf(service, sizeof(service) - 1, "%d", port);

    // Resolve the server address and service (e.g., port).
    addrinfo *addressInfo;
    int errorCode = getaddrinfo(0, service, &hints, &addressInfo);
    if (errorCode != 0 ) {
        cerr << "Call to getaddrinfo() failed, error=" << gai_strerror(errorCode) << endl;
        return -1;
    }

    // Create a socket for the server to listen on.
    int fd = socket(addressInfo->ai_family, addressInfo->ai_socktype, addressInfo->ai_protocol);
    if (fd == -1) {
        cerr << "Call to socket() failed, errno=" << errno << ", error=" << strerror(errno) << endl; 
        freeaddrinfo(addressInfo);
        return -1;
    }

    // Bind the IP address to the socket.
    int result = bind(fd, addressInfo->ai_addr, (socklen_t)addressInfo->ai_addrlen);
    if (result == -1) {
        cerr << "Call to bind() failed, errno=" << errno << ", error=" << strerror(errno) << endl; 
        freeaddrinfo(addressInfo);
        close(fd);
        return -1;
    } 

    // Finally, free the addressInfo.
    freeaddrinfo(addressInfo);

    return fd;
}

int TcpUtils::listenForConnections(int socket)
{

    // Listen for connections on a server socket.
    int result = listen(socket, SOMAXCONN);
    if (result == -1) {
       cerr << "Call to listen() failed, errno=" << errno << ", error=" << strerror(errno) << endl;
       close(socket);
       return result;
    }

    return result;
}

int TcpUtils::acceptConnection(int socket)
{

    // Accept client connection and return a client socket.
    int clientSocket = accept(socket, 0, 0);
    if (clientSocket == -1) {
        cerr << "Call to accept() failed, errno=" << errno << ", error=" << strerror(errno) << endl;
    }

    return clientSocket;
}

int TcpUtils::sendToPeer(int socket, void* buffer, size_t bufferLength, int flags)
{
    int result = send(socket, buffer, bufferLength, flags);
    if (result == -1) {
        cerr << "Call to send() failed, errno=" << errno << ", error=" << strerror(errno) << endl;
    }

    return result;
}

int TcpUtils::closeSocket(int socket)
{
    int result = close(socket);
    if (result == -1) {
        cerr << "Call to close() failed, errno=" << errno << ", error=" << strerror(errno) << endl;
    }

    return result;
}

