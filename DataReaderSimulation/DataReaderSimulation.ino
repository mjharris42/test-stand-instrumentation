#include <SerialBuffer.h>
#include "SensorData.h"

#define BUFFER_SIZE 64
byte buffer[BUFFER_SIZE];

SerialBuffer serialBuffer;

SensorData_t sensorData;

void setup()
{
  // Set up the buffer storage and maximum size.
  serialBuffer.buffer = buffer;
  serialBuffer.bufferSize = sizeof(buffer);
    
  // Reset the buffer.
  serialBuffer.reset();
    
  // Bring up the serial port.
  Serial.begin(SERIAL_BAUD);
}

void loop()
{
  delay(500);
  readData();
  writeData();
}

void readData()
{
  readLoads();
  readPressures();
  readTemperatures();
}

void readLoads()
{
  for (int i = 0; i < LOAD_CHANNELS; ++i)
  {
    sensorData.loads[i] = 4 + i;
  }
}

void readPressures()
{
  for (int i = 0; i < PRES_CHANNELS; ++i)
  {
    sensorData.pressures[i] = 14 + i;
  }
}

void readTemperatures()
{
  for (int i = 0; i < TEMP_CHANNELS; ++i)
  {
    sensorData.temperatures[i] = 20 + i;
  }
}

void writeData()
{
  
  // Package sensor data into the serial buffer.
  serialBuffer.startMessage();
  int valuesLength = sizeof(sensorData.values);
  byte* values = (byte*) sensorData.values;
  for (int i = 0; i < valuesLength; ++i)
  {
    serialBuffer.write(values[i]);
  }
  serialBuffer.endMessage();
  
  // Write buffer to the serial port.
  Serial.write(buffer, serialBuffer.messageLength());
  
}


