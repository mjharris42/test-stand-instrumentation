#ifndef SENSOR_DATA_H
#define SENSOR_DATA_H

#include "channels.h"

typedef union
{
  struct
  { 
    int16_t loads[LOAD_CHANNELS];
    int16_t pressures[PRES_CHANNELS];
    int16_t temperatures[TEMP_CHANNELS];
  };
  int16_t values[CHANNELS];
} SensorData_t;

#endif
