const int ledPin = 13;

void setup(){
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  if (Serial.available())  {
     byte command = Serial.read();
     // Serial.write(command);
     if (command == 'r') {
       blink(1);
     }
     else if (command == 'c') {
       blink(3);
     }
     else {
       blink(9);
     }
     Serial.write(command);
  }
  // blink(1);
  delay(500);
}

void blink(int numberOfTimes){
  for (int i = 0; i < numberOfTimes; i++)  {
    digitalWrite(ledPin, HIGH);
    delay(100);
    digitalWrite(ledPin, LOW);
    delay(100);
  }
}
