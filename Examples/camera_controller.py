#!/usr/bin/python

import time
import serial

def openSerial():
    ser = serial.Serial('/dev/ttyACM0', 9600)
    time.sleep(2)
    return ser

def closeSerial(ser):
    ser.close()

def sendCommand(ser, command):
    ser.write(command)
    while 1:
        if ser.inWaiting():
            response = ser.read()
            print "Response: " + response
            break
        time.sleep(2)

def help():
    print "Commands are r (reset), c (cpature), h (help), q (quit)" 

# main code starts here

ser = openSerial()

while 1:
    command = raw_input("Command (r, c, h, q): ")
    if command == 'q':
        break
    elif command == 'r':
        sendCommand(ser, command)
    elif command == 'c':
        sendCommand(ser, command)
    elif command == 'h':
        help()
    else:
        print "Unknown command: " + command

closeSerial(ser)
