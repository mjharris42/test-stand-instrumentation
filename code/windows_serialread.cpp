#define LOG_LEVEL plat::LOG_DEBUG
#include "Ws2tcpip.h"
#include "chr_winplatform.h"

void* Alloc(size_t BytesToAlloc, bool32 ZeroTheMemory) { return plat::Alloc(BytesToAlloc, ZeroTheMemory); }
bool32 Free(void* Memory) { return plat::Free(Memory); } 

#ifndef NAN
	static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
	#define NAN (*(const float *) __nan)
#endif

#undef Assert
#define Assert(Statement) !(Statement) && (LogF(plat::LOG_ERROR, "!! ASSERTION FAILED !! " #Statement " at " __FILE__ ": %d\n", __LINE__)); 

HANDLE
OpenSerialPort(char* PortName)
{
	HANDLE SerialHandle;

	string SerialNameStr = FormatString("\\\\.\\%s", PortName); 
	char* SerialName = SerialNameStr.Value;

	SerialHandle = CreateFile(SerialName, GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

	if (SerialHandle == INVALID_HANDLE_VALUE){
		LogF(plat::LOG_ERROR, "%s not a valid handle.\n", SerialName);
		if (GetLastError() == ERROR_FILE_NOT_FOUND){
			CloseHandle(SerialHandle);
			return INVALID_HANDLE_VALUE;	
		}
		CloseHandle(SerialHandle);
		return INVALID_HANDLE_VALUE;	
	}

	DCB dcbSerialParams = { 0 };
	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	if (!GetCommState(SerialHandle, &dcbSerialParams)){
		LogF(plat::LOG_ERROR, "Couldn't get %s state.\n", SerialName);
		CloseHandle(SerialHandle);
		return INVALID_HANDLE_VALUE;	
	}

	dcbSerialParams.BaudRate = CBR_9600;
	dcbSerialParams.ByteSize = 8;
	dcbSerialParams.StopBits = ONESTOPBIT;
	dcbSerialParams.Parity = NOPARITY;

	if (!SetCommState(SerialHandle, &dcbSerialParams)){
		LogF(plat::LOG_ERROR, "Couldn't set %s state.\n", SerialName);
		CloseHandle(SerialHandle);
		return INVALID_HANDLE_VALUE;	
	}

	COMMTIMEOUTS timeouts = { 0 };
	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutConstant = 50;
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = 50;
	timeouts.WriteTotalTimeoutMultiplier = 10;
	if (!SetCommTimeouts(SerialHandle, &timeouts)){
		LogF(plat::LOG_ERROR, "Couldn't set %s timeouts.\n", SerialName);
		CloseHandle(SerialHandle);
		return INVALID_HANDLE_VALUE;	
	}

	return SerialHandle;
}

void AppendToFile(HANDLE FileHandle, string Text)
{
	DWORD BytesWritten;
	WriteFile(FileHandle, Text.Value, (DWORD)Text.Length, &BytesWritten, 0);
}

bool32 ReadFromFile(HANDLE FileHandle, char* Buffer, uint32 BytesToRead)
{
	DWORD BytesRead;
	return ReadFile(FileHandle, Buffer, BytesToRead, &BytesRead, NULL);
}

static bool
InitializeSockets()
{
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) {
		LogF(plat::LOG_WARNING, "WSAStartup failed with error: %d\n", iResult);
	}
	return iResult == 0;
}

static void 
CleanupSockets()
{
	WSACleanup();
}

int
GetSocketError()
{
	return WSAGetLastError();
}

#include "serialread.cpp"

int WindowsMain(int argc, char* argv[])
{
	char* Hostname = "localhost";
	char* Port = "8888";
	char* SerialPort = "COM3";
	// if argc == 1, argv is just the name of the thing called (no args)
	if (argc >= 2) { Hostname = argv[1]; }
	if (argc >= 3) { Port = argv[2]; }
	if (argc >= 4) { SerialPort = argv[3]; }

	TestDummyData(Hostname, Port);
#if 0

	HANDLE SerialHandle = OpenSerialPort(SerialPort);
	HANDLE DataFile = CreateFile("serial.dat", FILE_APPEND_DATA, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (DataFile == INVALID_HANDLE_VALUE) 
	{
		LogF(plat::LOG_ERROR, "Couldn't open serial.dat for writing.\n");
		return -1; 
	}

	SerialReadWriteLoop(SerialHandle, DataFile);
	CloseHandle(SerialHandle);
	CloseHandle(DataFile);
#endif
	return 0;
}

