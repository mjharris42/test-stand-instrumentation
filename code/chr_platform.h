#if !defined(CHR_PLATFORM)

#include "chr.h"
#include "chr_string.h"

#define PrintFC plat::ColorPrintFormatted
#define LogF plat::LogFormatted

#if !defined(LOG_LEVEL)
	#define LOG_LEVEL plat::LOG_WARNING
#endif
#if !defined(LOG_LEVEL_PRINT)
	#define LOG_LEVEL_PRINT LOG_LEVEL
#endif
#if !defined(LOG_LEVEL_WRITE)
	#define LOG_LEVEL_WRITE LOG_LEVEL
#endif

/* 
	Struct definitions
*/
namespace plat
{
	struct time
	{
		uint32 Microsecond;
		uint16 Millisecond;
		uint16 Year;
		uint16 YearDay;		// Ordinal
		uint8  Month;		// Ordinal (1 = January, 2 = February, etc)
		uint8  MonthDay;	// Ordinal
		uint8  WeekDay;		// Ordinal (1 = Monday, 2 = Tuesday, 3 = Wednesday, etc)
		uint8  Hour;
		uint8  Minute;
		uint8  Second;
	};

	// NOTE(chronister): This uses the Windows convention of 100-nanosecond intervals.
	//	 That is, 1 unit of raw time == 100 nanoseconds.
	typedef uint64 rawtime;

	struct read_file_result
	{
		size_t ContentsSize;
		void* Contents;
	};

	struct cstring_stack
	{
		char** Values;
		uint32 Count;
		uint32 Capacity;
	};

	enum log_level
	{
		LOG_DEBUG,
		LOG_INFO,
		LOG_WARNING,
		LOG_ERROR
	};

	char* LogLevelNames[4] = { "DEBUG", "INFO", "WARNING", "ERROR" };
	char* LogLevelColoredNames[4] = { "_rgb|-`DEBUG`", "|g`INFO`", "|rg`WARNING`", "|r`ERROR`" };
}

/*
	Forward declared functions that will be implemented by each platform
*/
namespace plat
{
	internal void* 
	Alloc(size_t BytesToAlloc, bool32 ZeroTheMemory = false);

	internal bool32
	Free(void* Memory);

	/* =======================
		Standard Input/Output
	   ======================= */

	internal bool32
	ColorPrint(size_t Length, char* String);

	internal bool32
	ColorPrintFormatted(char* FormatString, ...);

	internal string 
	ReadLine();

	internal int 
	LogSimple(char* String);

	internal int	
	Log(log_level Level, char* String);

	internal int	
	LogFormatted(log_level Level, char* FormatString, ...);

	/* ==================
		   Filesystem 
	   ================== */

	internal string
	GetCurrentDirectory();

	internal bool32
	SetCurrentDirectory(char* NewDirectory);

	internal char*
	GetFileName(size_t PathLength, char* Path);

	internal bool32
	FindInDirectory(char* Path, char* Search);

	internal bool32
	PathExists(char* Path);

	internal bool32
	PathIsFolder(char* Path);

	internal bool32
	CreateDirectory(char* Path);

	internal string*
	ListFilesInDirectory(char* Path, int* NumberOfListedFiles);

	internal void
	FreeFile(read_file_result File);

	internal read_file_result
	ReadFile(char* Filename, int32 BytesToRead);

	internal read_file_result
	ReadEntireFile(char* Filename);

	internal bool32
	AppendToFile(char* Filepath, size_t StringSize, char* StringToAppend);

	bool32
	internal WriteEntireFile(char* Filepath, size_t StringSize, char* StringToWrite);

	internal bool32
	FileExists(char* Filepath);

	internal string
	GetUserDir();

	/* =====================
		 Command execution 
	   =====================*/

	internal void
	RunCommand(char* CommandLine);

	internal string
	RunCommandCaptureOutput(char* CommandLine, int CharsToCapture);

	/* =====================
		  Time and Timing 
	   =====================*/

	internal plat::time
	RawTimeToPlatformTime(rawtime Time);
	
	internal rawtime 
	GetTimestamp();

	internal plat::time
	GetTimeUniversal();

	internal plat::time
	GetTimeUniversal(rawtime Timestamp);

	internal plat::time
	GetTimeLocal();

	internal plat::time
	GetTimeLocal(rawtime Timestamp);
}

/*
	Functions defined and implemented here portably.
*/
namespace plat 
{
	void
	PushOntoCStringStack(cstring_stack* Stack, char* String)
	{
		if (Stack->Values == null)
		{
			Stack->Capacity = 10;
			Stack->Values = (char**)Alloc(Stack->Capacity*sizeof(char*), false);
		}

		if (Stack->Count + 1 >= Stack->Capacity)
		{
			Stack->Capacity += 10;

			char** Old = Stack->Values;
			Stack->Values = (char**)Alloc(Stack->Capacity*sizeof(char*), false);
			uint32 BytesToCopy = Stack->Count * sizeof(char*);
			CopyMemory(Stack->Values, Old, BytesToCopy);
			Free(Old);
		}

		Stack->Values[Stack->Count++] = String;
	}

	char*
	PopOffOfCStringStack(cstring_stack* Stack)
	{
		char* Item = Stack->Values[--Stack->Count];
		Stack->Values[Stack->Count] = 0;
		return Item;
	}

	//TODO(chronister): Need some kind of program/thread context, probably
	global_variable cstring_stack GlobalDirectoryStack;

	bool32
	PushDirectory(char* NewDirectory)
	{
		string Current = GetCurrentDirectory();
		PushOntoCStringStack(&GlobalDirectoryStack, Current.Value);
		return SetCurrentDirectory(NewDirectory);
	}

	bool32
	PopDirectory()
	{
		char* Popped = PopOffOfCStringStack(&GlobalDirectoryStack); 
		bool32 Result = SetCurrentDirectory(Popped);
		Free(Popped);
		return Result;
	}

	bool32 
	ConfirmAction(char* Prompt)
	{
		int32 Result = -1;
		string Response;

		do {
			PrintFC("%s >> ", Prompt);
			Response = plat::ReadLine();
			LowercaseString(Response.Value);
			//Let's test a truly rediculous number of options.
			if (CompareStrings(Response.Value, "y")
			 || CompareStrings(Response.Value, "yes")
			 || CompareStrings(Response.Value, "yeah")
			 || CompareStrings(Response.Value, "ok")
			 || CompareStrings(Response.Value, "okay"))
			{
				Result = 1;
			}
			else if (CompareStrings(Response.Value, "n")
			 || CompareStrings(Response.Value, "no")
			 || CompareStrings(Response.Value, "nah")
			 || CompareStrings(Response.Value, "nope")
			 || CompareStrings(Response.Value, "quit")
			 || CompareStrings(Response.Value, "q"))
			{
				Result = 0;
			}
			else
			{
				PrintFC("Yes or No (couldn't figure out |R`\"%s\"`), please try again.\n", Response.Value);
			}
		} while(Result < 0);

		return Result > 0;
	}
	
	/* Note on time formatting:
		The time format is similar to printf, with % being used as a format specifier.
		The accepted values for format specifiers are:
		 - %[n]y   Year, where n is the number of digits to print (usually 2 or 4, 4 if unspecified).
		 - %[Ss]m  Month. If s is specified, use the shortened month name. If S, use the full month name.
		 - %d	   Day of month.
		 - %D	   Day of year.
		 - %[Ss]w  Day of week. If s is specified, use the shortened day name. If S, use the full day name. 
		 - %[s]H   Hour (24-hour time).
		 - %[s]h   Hour (12-hour time).
		 - %[s]M   Minute.
		 - %[s]s   Second.
		 - %[n]S   "Fractions" of second, where n is the number of digits to print (starting from the left). 
					 i.e., 1 for tenths, 2 for hundreths, 3 for thousandths ( == milliseconds)

		For numeric quantities, if [s] is present before the format specifier, use the word form of the number.
		The number of milliseconds is between 0 and 999.
	*/
	bool32
	IsTimeFormatSpecifier(char C)
	{
		return (C == 'y' || C == 'm' || C == 'd' || C == 'D' || C == 'w' 
			 || C == 'H' || C == 'h' || C == 'M' || C == 's' || C == 'S' 
			 || IsNumber(C));
	}

	string 
	GetFirstNDigitsOf(int Num, uint N)
	{
		string Result = AllocateString(N + 1);

		int Length = Log10(Num) + 1;
		int TargetLength = N; 
		if (Length <= TargetLength)
		{
			while (Length < TargetLength)
			{
				AppendToString(&Result, STR("0"));
				++Length;
			}
			AppendFormatIntoString(&Result, "%d", Num);
		}
		else
		{
			while (Log10(Num) + 1 > TargetLength)
			{
				Num /= 10;
			}	
			AppendFormatIntoString(&Result, "%d", Num);
		}
		return Result;
	}

	void
	FormatTimeInto(string* Result, char* FormatString, plat::time Time)
	{
		FormatString = DuplicateCString(FormatString);
		uint32 FormatLength = StringLength(FormatString);

		string Buffer = AllocateString(FormatLength + 64); // TODO(chronister): double-check this number

		uint32 FormatCursor = 0, FormatStart = 0, FormatEnd = 0;
		bool32 InFormatSpecifier = false;
		for (FormatCursor; 
			 FormatCursor < FormatLength + 1;
			 ++FormatCursor)
		{
			if (InFormatSpecifier)
			{
				if (!IsTimeFormatSpecifier(FormatString[FormatCursor]))
				{
					InFormatSpecifier = false;
					FormatEnd = FormatCursor;

					char FormatCharacter = FormatString[FormatCursor - 1];

					switch(FormatCharacter)
					{
						case 'y':
						{
							// Year format. TODO(chronister): Digit specifier
							string NumberDigits = GetFirstNDigitsOf(Time.Year, 4);
							AppendToString(&Buffer, NumberDigits);
							FreeString(&NumberDigits);
						} break;

						case 'm':
						{
							// Month format. TODO(Chronister): Long forms
							AppendFormatIntoString(&Buffer, "%02d", Time.Month+1);
						} break;

						case 'd':
						{
							// Day of month format.
							AppendFormatIntoString(&Buffer, "%02d", Time.MonthDay);
						} break;

						case 'D':
						{
							// Day of year format.
							AppendFormatIntoString(&Buffer, "%d", Time.YearDay);
						} break;

						case 'w':
						{
							// Day of week format. TODO(chronister): Long forms
							AppendFormatIntoString(&Buffer, "%d", Time.WeekDay);
						} break;

						case 'H':
						{
							// 24-Hour format. TODO(chronister): Long forms
							AppendFormatIntoString(&Buffer, "%02d", Time.Hour);
						} break;

						case 'h':
						{
							// 12-hour format. TODO(chronister): Long forms
							uint8 Hour = (Time.Hour % 12);
							if (Hour == 0) { Hour = 12; }
							AppendFormatIntoString(&Buffer, "%d", Hour);
						} break;

						case 'M':
						{
							// Minute format. TODO(chronister): Long forms
							AppendFormatIntoString(&Buffer, "%02d", Time.Minute);
						} break;

						case 's':
						{
							// Second format. TODO(chronister): Long forms
							AppendFormatIntoString(&Buffer, "%02d", Time.Second);
						} break;

						case 'S':
						{
							// Fraction of second format. TODO(chronister): Length specifier
							AppendFormatIntoString(&Buffer, "%03d", Time.Millisecond);
						} break;

						case '%':
						{
							// Literal percent. Just print it.
							AppendToString(&Buffer, STR("%"));
						}

						default:
						{
							// Not an accepted character. Ignore this format string. 
						} break;
					}
				}
			}
			else
			{
				if (FormatString[FormatCursor] == '%')
				{
					InFormatSpecifier = true;
					FormatStart = FormatCursor;
					FormatString[FormatCursor] = '\0';
					if (FormatCursor > 0) { AppendToString(&Buffer, STR(FormatString + FormatCursor - 1)); }
				}
			}
		}
		AppendToString(&Buffer, STR(FormatString + FormatEnd)); 

		CopyString(Buffer.Length, Buffer.Value, Result->Capacity - 1, Result->Value, true);
		Result->Length = Min(Buffer.Length, Result->Capacity - 1);
		FreeString(&Buffer);
		Free(FormatString);
	}

	string
	FormatTime(char* FormatString, plat::time Time)
	{
		string Result = AllocateString(StringLength(FormatString) + 64);
		FormatTimeInto(&Result, FormatString, Time);
		return Result;
	}

	internal int	
	Log(log_level Level, char* String)
	{
		if (Level < LOG_LEVEL_WRITE && Level < LOG_LEVEL_PRINT) { return 0; }

		string BuiltString = AllocateString(StringLength(String) + 128);
		string TimeString = FormatTime("%y-%m-%d %h:%M:%s.%S", GetTimeLocal());
		
		string FormatSafe = STR(String);
		StringReplace(&FormatSafe, STR("%"), STR("%%"));

		if (Level >= LOG_LEVEL_WRITE)
		{
			FormatIntoString(&BuiltString, "%s\t%s\t%s", TimeString.Value, LogLevelNames[Level], FormatSafe.Value);
			LogSimple(BuiltString.Value);
		}
		if (Level >= LOG_LEVEL_PRINT)
		{
			FormatIntoString(&BuiltString, "_rgb|-`%s`\t%s\t%s", TimeString.Value, LogLevelColoredNames[Level], FormatSafe.Value);
			ColorPrint(BuiltString.Length, BuiltString.Value);
		}
		FreeString(&BuiltString);

		FreeString(&FormatSafe); // Alloc in overload of StringReplace
		return 0;
	}

	int	
	LogFormatted(log_level Level, char* FormatString, ...)
	{
		va_list args;
		va_start(args, FormatString);
		string FormattedString = FormatStringVariadic(FormatString, args);
		Log(Level, FormattedString.Value);
		FreeString(&FormattedString);
		va_end(args);

		return 0;
	}
}

#define CHR_PLATFORM
#endif
