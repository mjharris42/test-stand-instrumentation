# How to use this file:
#  python setup.py bdist
#  This will use cx_freeze to compile all of the dependencies into one directory and pack the script into a .exe file.

#  This is really nice for usability reasons. However, all the dependencies are still all over the place, so for best
#  results, you also want to use a built in windows program called IExpress.

#  Once the build is done, you should have a directory called build/exe.win-amd64-3.4 or something like that. Open
#  IExpress on the command line (type in the command there, it will open a GUI window) and load the vis.SED file. 

#  Click create package and wait for it to complete. It will make a nice self-extracting exe that puts everything it
#  needs in a temp directory before running.

import sys
from cx_Freeze import setup, Executable

options = {
    'build_exe': {
        'compressed': True,
        'includes': [
            'PySide',
            'pyqtgraph'
        ],
        'excludes': [
            'matplotlib'
        ],
        'path': sys.path + ['modules']
    }
}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

executables = [
    Executable('vis.py', base=base),
]

setup(name='vis',
      version='0.1',
      description='Test stand instrumentation',
      options=options,
      executables=executables
      )
