#ifndef THERMOCOUPLE_H
#define THERMOCOUPLE_H

typedef uint8_t ScratchPad_t[9];
typedef uint8_t DeviceAddress_t[9];

// Everything to do with the thermocouple ADCs. 
namespace Thermocouple
{
  
  // Class to interface with the thermocouples
  #define ONE_WIRE_BUS 7
  OneWire onewire(ONE_WIRE_BUS);
  
  // OneWire commands
  #define STARTCONVO      0x44  // Tells device to take a temperature reading and put it on the scratchpad
  #define COPYSCRATCH     0x48  // Copy EEPROM
  #define READSCRATCH     0xBE  // Read EEPROM
  #define WRITESCRATCH    0x4E  // Write to EEPROM
  #define RECALLSCRATCH   0xB8  // Reload from last known
  
  // Scratchpad locations
  #define TEMP_LSB        0
  #define TEMP_MSB        1
  #define INT_TEMP_LSB 	2
  #define INT_TEMP_MSB 	3
  #define CONFIGURATION   4
  #define RESERVED_1 		5
  #define RESERVED_2 		6
  #define RESERVED_3		7
  #define SCRATCHPAD_CRC  8

  bool validAddress(uint8_t* deviceAddress)
  {
    return (onewire.crc8(deviceAddress, 7) == deviceAddress[7]);
  }

  bool getAddress(uint8_t* deviceAddress, uint8_t index)
  {
    uint8_t depth = 0;
    onewire.reset_search();
    while (depth <= index && onewire.search(deviceAddress))
    {
      if (depth == index && validAddress(deviceAddress)) return true;
      depth++;
    }

    return false;
  }

  void readScratchPad(uint8_t* deviceAddress, uint8_t* scratchPad)
  {
    onewire.reset();
    onewire.select(deviceAddress);
    onewire.write(READSCRATCH);

    // byte 0: temperature LSB and fault status
    // byte 1: temperature MSB
    // byte 2: internal temp LSB
    // byte 3: internal temp MSB
    // byte 4: configuration register
    // byte 5: RESERVED
    // byte 6: RESERVED
    // byte 7: RESERVED
    // byte 8: CRC
    //
    for(int i=0; i<9; i++)
    {
      scratchPad[i] = onewire.read();
    }

    for (uint8_t i=0; i<8; i++) {
      //Serial.print("\n 0x"); Serial.print(scratchPad[i], HEX);
    }
    onewire.reset();
  }

  // Tells the ADCs to start preparing a value for us. Takes 72 milliseconds after this happens to get a reading.
  void startConversion(uint8_t* deviceAddress)
  {
    onewire.reset();
    onewire.select(deviceAddress);
    onewire.write(STARTCONVO, false);
  }  

  void test()
  {
    ScratchPad_t scratchPad;
    uint8_t tempDA[8];
    onewire.reset_search();
    int knownDevices = 0;
    while (onewire.search(tempDA))
    {
      if (validAddress(tempDA))
      {
        readScratchPad(tempDA, scratchPad);
        ++knownDevices;
      }
    }
    Serial.print("Found ");
    Serial.print(knownDevices);
    Serial.print(" devices.\n");
  }
  
}

#endif
