#include <SerialBuffer.h>
#include <Wire.h>
#include <OneWire.h>
#include <Adafruit_ADS1015.h>

#include "Thermocouple.h"

#include "SensorData.h"

#define BUFFER_SIZE 64
byte buffer[BUFFER_SIZE];

SerialBuffer serialBuffer;

SensorData_t sensorData;

// Class to interface with the pressure sensor ADC
Adafruit_ADS1115 ads;

// Thermocouple variables
uint8_t currentDevice = 0;
unsigned long lastTemp = 0;
ScratchPad_t scratchPad;
int16_t rawTemps[TEMP_CHANNELS];

void setup()
{
  // Set up the buffer storage and maximum size.
  serialBuffer.buffer = buffer;
  serialBuffer.bufferSize = sizeof(buffer);
    
  // Reset the buffer.
  serialBuffer.reset();
    
  // Bring up the serial port.
  Serial.begin(SERIAL_BAUD);
  
  // Test thermocouples.
  //Thermocouple::test();
  
  // Set up pressure sensors.
  ads.begin();
}

void loop()
{
  readData();
  writeData();
}

void readData()
{
  readLoads();
  readPressures();
  readTemperatures();
}

void readLoads()
{
  for (int i = 0; i < LOAD_CHANNELS; ++i)
  {
    sensorData.loads[i] = analogRead(A0 + i);
  }
}

void readPressures()
{
  for (int i = 0; i < PRES_CHANNELS; ++i)
  {
    sensorData.pressures[i] = ads.readADC_Differential_0_1();
  }
}

void readTemperatures()
{
  // There are (at least) two ways to poll this:
  //  1: All at once, after polling the analog sensors. This is slow and will
  //  introduce delay to the whole system.
  //  2: One sensor per analog poll. This means that at any given time, data from
  //  4 of the channels are stale. However, we can poll e.g. the load cell more
  //  frequently.

  // Note: this is actually not the most efficient way to do this, I think. Each device needs to wait 72 ms, but the
  // delay between switching devices is much shorter. It might be possible to query all of the devices at once with a
  // microsecond-factor delay between devices to account for onewire signalling.
  if (millis() - lastTemp > 72)
  {
    DeviceAddress_t deviceAddress;
    if (Thermocouple::getAddress(deviceAddress, currentDevice))
    {
      Thermocouple::readScratchPad(deviceAddress, scratchPad);

      rawTemps[currentDevice] = (((int16_t)scratchPad[TEMP_MSB]) << 8) | scratchPad[TEMP_LSB];
      rawTemps[currentDevice] >>= 2;
      // These devices have an internal thermocouple too, but we don't need to read those right now.
      // InternalTemps[currentDevice] = (((int16_t)scratchPad[INT_TEMP_MSB]) << 8) | scratchPad[INT_TEMP_LSB];
      // InternalTemps[currentDevice] >>= 4;

      lastTemp = millis();
      currentDevice = (++currentDevice) % TEMP_CHANNELS;
      Thermocouple::startConversion(deviceAddress);
    }
    else
    {
      // Couldn't poll one of the thermocouples, skip it and move on.
      currentDevice = (++currentDevice) % TEMP_CHANNELS;
    }
  }

  for (int i = 0; i < TEMP_CHANNELS; ++i)
  {
    sensorData.temperatures[i] = rawTemps[i];
  }
}

void writeData()
{
  
  // Package sensor data into the serial buffer.
  serialBuffer.startMessage();
  int valuesLength = sizeof(sensorData.values);
  byte* values = (byte*) sensorData.values;
  for (int i = 0; i < valuesLength; ++i)
  {
    serialBuffer.write(values[i]);
  }
  serialBuffer.endMessage();
  
  // Write buffer to the serial port.
  Serial.write(buffer, serialBuffer.messageLength());
  
}

