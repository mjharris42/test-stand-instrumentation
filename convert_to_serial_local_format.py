#!/usr/bin/python

from sys import argv

script, fileName = argv

with open(fileName, "r") as inData:
    for inLine in inData:
        inLine = inLine.strip()
        inLine = '\0' + inLine
        print inLine
